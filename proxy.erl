-module(proxy).
-behaviour(gen_server).
-compile(export_all).
-record(state,{noConn}).

start_link() -> gen_server:start_link(?MODULE, [], []).
getIP(Pid,IP,Port) ->
	gen_server:call(Pid,{IP,Port}).

init([]) -> {ok, []}.
handle_call({IP,Port}, _From, NoConn) ->
%%	Reply = io:format("rcvd:~nIP ~p ~nPort ~p ~n",[IP,Port]),  %%save the output of request in Reply
	inets:start(),
	Reply = http:request(post, { IP, [], "application/x-www-form-urlencoded", "hl=en&q=erlang&btnG=Google+Search&meta=" }, [], []),
	{reply,Reply,NoConn}.

checkIP(Pid,IP,Port) ->
	io:format("rcvd:~nIP ~p ~nPort ~p ~n",[IP,Port]),
	{reply,ok}.
